(function() {
  'use strict';

  angular
    .module('project')
    .config(routerConfig);

  /** @ngInject */
  function routerConfig($stateProvider, $urlRouterProvider) {
    $stateProvider
      .state('home2', {
        url: '/home_es',
        templateUrl: 'app/main/main_es.html',
        controller: 'MainController',
        controllerAs: 'home'
      }).state('session', {
        url: '/session',
        templateUrl: 'app/session/session.html',
        controller: 'SessionController',
        controllerAs: 'session'
      }).state('register', {
        url: '/register',
        templateUrl: 'app/register/register.html',
        controller: 'RegisterController',
        controllerAs: 'register'
      })

    $urlRouterProvider.otherwise('/home_es');
  }
})();