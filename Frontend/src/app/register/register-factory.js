'use strict';

angular
  .module('project')
  .factory('RegisterFact', function ( $http, $window, $state, URL, $rootScope, $stateParams, toastr) {

    return {
      access: function (user, callback) {
        
        $http({
          method: 'post',
          url: URL+'user',
          data: user
        })
        .success(function (data) {
          if (callback) callback(data);
          $rootScope.user = data;
          //console.log(data);
          toastr.success('Bienvenido a nuestra app '+data.name+'!');
          //window.localStorage.setItem('cdsl', data.code_sl.code);
          $state.go('home2');
        })
        .error(function () {
          toastr.error('Hay datos erroneos');
        })
      },
      getCountries: function (callback) {

        $http({
          method: 'get',
          url: URL+'country'
        })
        .success(function (data) {
          if (callback) callback(data);
          $rootScope.countries = data;
          //console.log(data);
        })
        .error(function () {
          toastr.error('Error');
        })
      }      
    }

  });