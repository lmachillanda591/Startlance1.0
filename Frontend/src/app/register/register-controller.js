(function() {
  'use strict';

  angular
    .module('project')
    .controller('RegisterController', RegisterController);

  /** @ngInject */
  function RegisterController($timeout, webDevTec, toastr, $scope, $anchorScroll, $location, RegisterFact) {
    var vm= this;
    $scope.user = {};
    $scope.Access = Access;
    $scope.getCountries = getCountries;

    function Access () {
      RegisterFact.access($scope.user, function () {
        return ".";
      });
    }

    function getCountries () {
      RegisterFact.getCountries(function () {
        return ".";
      });
    }

      $(document).ready(function() {
        // create DatePicker from input HTML element
        $("#datepicker").kendoDatePicker();
        $("#monthpicker").kendoDatePicker({
          // defines the start view
          start: "year",
          // defines when the calendar should return date
          depth: "year",
          // display month and year in the input
          format: "MMMM yyyy"
      });
      $("#uk").click(function(){
        $("#tokio").show(10)
        $(this).hide(10)
      });
      $("#tokio").click(function(){
        $(this).hide(10)
        $("#paris").show(10)
      });
      $("#paris").click(function(){
        $(this).hide(10)
        $("#idk").show(10)
      });
      $("#idk").click(function(){
        $(this).hide(10)
        $("#uk").show(10)
      });
    }); // OJO : LA FECHA NO LA TOMA LA DB
  }
})();