(function() {
  'use strict';

  angular
    .module('project')
    .controller('SessionController', SessionController);

  /** @ngInject */
  function SessionController($timeout, webDevTec, toastr, $scope, $anchorScroll, $location, SessionFactory) {
    var vm= this;
    $scope.session = {};
    $scope.Access = Access;

    function Access () {
      SessionFactory.access($scope.session, function () {
        return ".";
      });
    }

    $(document).ready(function(){
      $("#close-btn").mouseenter(function(){
        $(this).fadeTo("fast", 1)
      });
      $("#close-btn").mouseleave(function(){
        $(this).fadeTo("fast", 0.8)
      });
      $("#uk").click(function(){
        $("#tokio").show(10)
        $(this).hide(10)
      });
      $("#tokio").click(function(){
        $(this).hide(10)
        $("#paris").show(10)
      });
      $("#paris").click(function(){
        $(this).hide(10)
        $("#idk").show(10)
      });
      $("#idk").click(function(){
        $(this).hide(10)
        $("#uk").show(10)
      });
    });
  }
})();