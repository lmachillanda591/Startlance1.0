'use strict';

angular
  .module('project')
  .factory('SessionFactory', function ($http, $window, $state, URL, $rootScope, $stateParams, toastr) {

    return {
      access: function (session, callback) {
        
        $http({
          method: 'post',
          url: URL+'session',
          data: session
        })
        .success(function (data) {
          if (callback) callback(data);
          $rootScope.user = data;
          toastr.success('Nos alegramos de que hayas vuelto '+data.name+'!');
          window.localStorage.setItem('cdsl', data.code_sl.code);
          $state.go('home2');
        })
        .error(function () {
          toastr.error('Correo o contraseña erroneas, se más cuidadoso/a!');
        });
      }
    }
  });