(function() {
  'use strict';

  angular
    .module('project')
    .controller('MainController', MainController);

  /** @ngInject */
  function MainController($timeout, webDevTec, toastr, $scope, $anchorScroll, $location) {

    var vm= this;

    vm.awesomeThings = [];
    vm.classAnimation = '';
    vm.creationDate = 1456451119842;
    vm.showToastr = showToastr;

    activate();

    function activate() {
      getWebDevTec();
      $timeout(function() {
        vm.classAnimation = 'rubberBand';
      }, 4000);
    }

    function showToastr() {
      toastr.info('Fork <a href="https://github.com/Swiip/generator-gulp-angular" target="_blank"><b>generator-gulp-angular</b></a>');
      vm.classAnimation = '';
    }

    function getWebDevTec() {
      vm.awesomeThings = webDevTec.getTec();

      angular.forEach(vm.awesomeThings, function(awesomeThing) {
        awesomeThing.rank = Math.random();
      });
    }
    
    var elem = document.querySelectorAll('.malarkeyOne')[0];
      var opts = {
      typeSpeed: 50,
      deleteSpeed: 50,
      pauseDelay: 1000,
      loop: true,
      postfix: ''
    };
      
    var m = malarkey(elem, opts)
    .type('We are creative').pause().delete()
    .type('We are connecting').pause().delete()
    .type('We are startlance').pause().delete();
    elem.style.color = '#03C9A9';

    // Menu settings
    $('#menuToggle, .menu-close').on('click', function(){
      $('#menuToggle').toggleClass('active');
      $('body').toggleClass('body-push-toleft');
      $('#theMenu').toggleClass('menu-open');
    });
    $(document).ready(function(){
      $(".social_icon").mouseenter(function(){
        $(this).fadeTo("fast", 1)
      });
      $(".social_icon").mouseleave(function(){
        $(this).fadeTo("fast", 0.8)
      });
      $(".btn-green").mouseenter(function(){
        $(this).fadeTo("fast", 1)
      });
      $(".btn-green").mouseleave(function(){
        $(this).fadeTo("fast", 0.8)
      });
      $("#vzla").mouseenter(function(){
        $(this).fadeTo("fast", 1)
      });
      $("#vzla").mouseleave(function(){
        $(this).fadeTo("fast", 0.8)
      });
    });
  }
})();