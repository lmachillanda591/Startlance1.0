class CreateSecurityQuestions < ActiveRecord::Migration
  def change
    create_table :security_questions do |t|
      t.string :question
      t.integer :value

      t.timestamps null: false
    end
  end
end
