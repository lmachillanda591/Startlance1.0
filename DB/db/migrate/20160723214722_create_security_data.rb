class CreateSecurityData < ActiveRecord::Migration
  def change
    create_table :security_data do |t|
      t.references :user, index: true, foreign_key: true
      t.references :security_question, index: true, foreign_key: true
      t.string :answer

      t.timestamps null: false
    end
  end
end
