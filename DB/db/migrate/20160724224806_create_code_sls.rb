class CreateCodeSls < ActiveRecord::Migration
  def change
    create_table :code_sls do |t|
      t.references :user, index: true, foreign_key: true
      t.string :code

      t.timestamps null: false
    end
  end
end
