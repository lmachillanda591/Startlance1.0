class CreateHeadHunters < ActiveRecord::Migration
  def change
    create_table :head_hunters do |t|
      t.references :user, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
