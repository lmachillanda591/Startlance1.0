class CreateOtherSecurityQuestions < ActiveRecord::Migration
  def change
    create_table :other_security_questions do |t|
      t.string :question
      t.string :answer
      t.references :user, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
