Rails.application.routes.draw do
  resources :user, only: [:create, :index, :show]
  resources :security_question, only: [:index, :show] 
  resources :security_data, only: [:create, :show]
  resources :other_security_question, only: [:create, :show]
  resources :head_hunter, only: [:create, :index, :show]
  resources :freelancer, only: [:create, :index, :show]
  resources :session, only: [:create, :destroy]
  resources :country, only: [:index, :show]
end