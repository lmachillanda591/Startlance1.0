class SecurityData < ActiveRecord::Base
  belongs_to :user
  belongs_to :security_question
end