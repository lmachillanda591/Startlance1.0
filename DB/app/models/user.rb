class User < ActiveRecord::Base
  has_one :session
  has_one :freelancer
  has_one :code_sl
  has_one :head_hunter
  has_one :security_data
  has_one :other_security_question
  before_save :encrypt_password
  after_save :create_code_sl
  validates :password, presence:true, length:{minimum:6, maximum:20}
  email_regex = /\A[\w+\-!%&=?'*+#*_.ñ-]+@[a-z\d\-.]+\.[a-z]+\z/i
  validates :email, presence:true, uniqueness: true, format: email_regex
  validates :username, presence:true, uniqueness: true, length:{minimum:4, maximum:12}
  accepts_nested_attributes_for :code_sl

  private
  ###---- Encrypt Password ----###
  def encrypt_password
    self.password = Digest::MD5.hexdigest(self.password)
  end

  def create_code_sl
    CodeSl.create(user_id: self.id, code: self.password + Digest::MD5.hexdigest(self.email))
  end
end