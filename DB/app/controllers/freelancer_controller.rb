class FreelancerController < ApplicationController
rescue_from ActiveRecord::RecordNotFound, with: :r_not_found

  def index
      @freelancer = Freelancer.all 
      render json: @freelancer
  end

  def show
    freelancer = Freelancer.find(params[:id])
    render json: freelancer.as_json
  end

  def create 
    freelancer = Freelancer.new(permit_params)
    if freelancer.save
      render json: freelancer
    end
  end

  def permit_params
    params.require(:freelancer).permit(:user_id)
  end
end