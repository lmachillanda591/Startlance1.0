class OtherSecurityQuestionController < ApplicationController
rescue_from ActiveRecord::RecordNotFound, with: :r_not_found

  def show
    other_security_question = OtherSecurityQuestion.find(params[:id])
    render json: other_security_question.as_json
  end

  def create 
    other_security_question = OtherSecurityQuestion.new(permit_params)
    if other_security_question.save
      render json: other_security_question
    end
  end

  def permit_params
    params.require(:other_security_question).permit(:question, :answer, :user_id)
  end
end