class SessionController < ApplicationController
  rescue_from ActiveRecord::RecordNotFound, with: :r_not_found

  def create
    s = Session.new(permit_params)
    code = search_cdsl(s.password, s.email)
    u = code.user.as_json(include: { code_sl: {only: :code} })
    if code 
      s.save
      render json: u
    end
  end
  
  private 
  def permit_params
    params.require(:session).permit(:id, :email, :password)
  end
  def search_cdsl(param1, param2)
    cdsl = CodeSl.where(code: Digest::MD5.hexdigest(param1) + Digest::MD5.hexdigest(param2)).take
    return cdsl
  end
end