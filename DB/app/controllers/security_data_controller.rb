class SecurityDataController < ApplicationController
rescue_from ActiveRecord::RecordNotFound, with: :r_not_found

  def show
    security_data = SecurityData.find(params[:id])
    render json: security_data.as_json
  end

  def create 
    security_data = SecurityData.new(permit_params)
    if security_data.save
      render json: security_data
    end
  end

  def permit_params
    params.require(:security_data).permit(:user_id, :security_question_id, :answer)
  end
end