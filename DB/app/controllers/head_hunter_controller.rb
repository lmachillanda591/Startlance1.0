class HeadHunterController < ApplicationController
rescue_from ActiveRecord::RecordNotFound, with: :r_not_found

  def index
      @head_hunter = HeadHunter.all 
      render json: @head_hunter
  end

  def show
    head_hunter = HeadHunter.find(params[:id])
    render json: head_hunter.as_json
  end

  def create 
    head_hunter = HeadHunter.new(permit_params)
    if head_hunter.save
      render json: head_hunter
    end
  end

  def permit_params
    params.require(:head_hunter).permit(:user_id)
  end
end