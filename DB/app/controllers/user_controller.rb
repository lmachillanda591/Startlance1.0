class UserController < ApplicationController
rescue_from ActiveRecord::RecordNotFound, with: :r_not_found

  def index
      @users = User.all 
      render json: @users
  end

  def show
    user = User.find(params[:id])
    render json: user.as_json
  end

  def create 
    user = User.new(permit_params)
    if user.save
      render json: user
    end
  end

  def permit_params
    params.require(:user).permit(:name, :last_name, :age, :id_number, :birth_date, :nationality, :email, :username, :password)
  end
end