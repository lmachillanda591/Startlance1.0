class CountryController < ApplicationController
rescue_from ActiveRecord::RecordNotFound, with: :r_not_found

  def index
      @countries = Country.all 
      render json: @countries
  end

  def show
    country = Country.find(params[:id])
    render json: country.as_json
  end

  def permit_params
    params.require(:country).permit(:country, :value)
  end
end