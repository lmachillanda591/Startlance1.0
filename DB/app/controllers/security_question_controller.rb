class SecurityQuestionController < ApplicationController
rescue_from ActiveRecord::RecordNotFound, with: :r_not_found

  def index
      @security_question = SecurityQuestion.all
      render json: @security_question
  end

  def show
    security_question = SecurityQuestion.find(params[:id])
    render json: security_question.as_json
  end

  def permit_params
    params.require(:security_question).permit(:question, :value)
  end
end